package fr.uavignon.ceri.tp4.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp4.data.database.CeriMuseumRoomDatabase;
import fr.uavignon.ceri.tp4.data.database.ItemDao;
import fr.uavignon.ceri.tp4.data.webservice.CMCollectionResponse;
import fr.uavignon.ceri.tp4.data.webservice.CMInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp4.data.database.CeriMuseumRoomDatabase.databaseWriteExecutor;

public class CMRepository {
    private static final String TAG = CMRepository.class.getSimpleName();
    private final CMInterface api;

    private LiveData<List<Item>> allItems;
    private MutableLiveData<Item> selectedItem;
    private MutableLiveData<Throwable> webServiceThrowable;
    private volatile int nbAPIloads = 0;

    private ItemDao itemDao;


    private static volatile CMRepository INSTANCE;

    public synchronized static CMRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new CMRepository(application);
        }

        return INSTANCE;
    }

    public CMRepository(Application application) {
        CeriMuseumRoomDatabase db = CeriMuseumRoomDatabase.getDatabase(application);
        itemDao = db.itemDao();
        allItems = itemDao.getAllItems();
        selectedItem = new MutableLiveData<>();
        webServiceThrowable = new MutableLiveData<>();

        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(CMInterface.class);
    }

    public void sortAlpha() {
        allItems = itemDao.getAllItemsAlpha();
    }

    public void sortChrono() {
        allItems = itemDao.getAllItemsChrono();
    }

    public void search(String search) {
        allItems = itemDao.searchItems(search);
    }

    public void reset() {
        allItems = itemDao.getAllItems();
    }

    public void getItem(String itemId) {
        Future<Item> fitem = databaseWriteExecutor.submit(() -> itemDao.getItem(itemId));
        try {
            selectedItem.setValue(fitem.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public LiveData<List<Item>> getAllitems() {
        return allItems;
    }

    public MutableLiveData<Item> getSelectedItem() {
        return selectedItem;
    }

    public MutableLiveData<Throwable> getWebServiceThrowable() {
        return webServiceThrowable;
    }

    public void updateCollection() {

        api.getCollection().enqueue(
                new Callback<Map<String, CMCollectionResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String, CMCollectionResponse>> call, Response<Map<String, CMCollectionResponse>> response) {
                        for(Map.Entry<String, CMCollectionResponse> entry: response.body().entrySet()) {
                            databaseWriteExecutor.submit(() -> {

                                Item item = itemDao.getItem(entry.getKey());
                                if(item == null) {
                                    itemDao.insert(new Item(
                                            entry.getKey(),
                                            entry.getValue().name,
                                            entry.getValue().categories,
                                            entry.getValue().description,
                                            entry.getValue().timeFrame,
                                            entry.getValue().year,
                                            entry.getValue().brand,
                                            entry.getValue().technicalDetails,
                                            entry.getValue().pictures,
                                            entry.getValue().working
                                    ));
                                }
                                else {
                                    item.setName(entry.getValue().name);
                                    item.setCategories(entry.getValue().categories);
                                    item.setDescription(entry.getValue().description);
                                    item.setTimeFrame(entry.getValue().timeFrame);
                                    item.setYear(entry.getValue().year);
                                    item.setBrand(entry.getValue().brand);
                                    item.setTechnicalDetails(entry.getValue().technicalDetails);
                                    item.setPictures(entry.getValue().pictures);
                                    item.setWorking(entry.getValue().working);
                                    itemDao.update(item);
                                }
                            });
                        }
                    }

                    @Override
                    public void onFailure(Call<Map<String, CMCollectionResponse>> call, Throwable t) {
                        webServiceThrowable.postValue(t);
                    }
                });
    }
}
