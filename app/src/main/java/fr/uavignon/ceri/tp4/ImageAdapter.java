package fr.uavignon.ceri.tp4;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;


import fr.uavignon.ceri.tp4.data.Item;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {
    private Item item;
    private SecondFragmentViewModel viewModel;
    private boolean layout_left = true;

    @NonNull
    @Override
    public ImageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageAdapter.ViewHolder holder, int position) {
        holder.imageInfo.setText(String.valueOf(item.getTransformedPictures().values().toArray()[position]));

        Glide.with(viewModel.getApplication())
                .load("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+item.getItemID()+"/images/"+item.getTransformedPictures().keySet().toArray()[position])
                .into(holder.imageThumb);
    }

    @Override
    public int getItemCount() {
        return item == null ? 0 : item.getTransformedPictures().size();
    }

    public void setItem(Item item) {
        this.item = item;
        notifyDataSetChanged();
    }

    public void setViewModel(SecondFragmentViewModel viewModel) {
        this.viewModel = viewModel;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView imageInfo;
        ImageView imageThumb;

        ViewHolder(View view) {
            super(view);

            imageInfo = view.findViewById(R.id.image_info);
            imageThumb = view.findViewById(R.id.image_thumb);
        }
    }
}