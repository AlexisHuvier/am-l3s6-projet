package fr.uavignon.ceri.tp4.data.database;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.squareup.moshi.JsonQualifier;

import java.util.List;

import fr.uavignon.ceri.tp4.data.Item;

@Dao
public interface ItemDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Item item);

    @Query("DELETE FROM item")
    void deleteAll();

    @Query("SELECT * from item")
    LiveData<List<Item>> getAllItems();

    @Query("SELECT * from item ORDER BY year")
    LiveData<List<Item>> getAllItemsChrono();

    @Query("SELECT * from item ORDER BY name")
    LiveData<List<Item>> getAllItemsAlpha();

    @Query("SELECT * from item WHERE itemID LIKE '%:search%' OR name LIKE '%' + :search + '%' OR categories LIKE '%' + :search + '%' OR description LIKE '%' + :search + '%' OR brand LIKE '%' + :search + '%' OR technicalDetails LIKE '%' + :search + '%'")
    LiveData<List<Item>> searchItems(String search);

    @Query("DELETE FROM item WHERE itemID = :itemID")
    void deleteitem(String itemID);

    @Query("SELECT * FROM item WHERE itemID = :itemID")
    Item getItem(String itemID);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Item city);

}
