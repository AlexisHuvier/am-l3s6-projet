package fr.uavignon.ceri.tp4.data.webservice;

import java.util.List;
import java.util.Map;

public class CMCollectionResponse {
    public String name;
    public List<String> categories;
    public String description;
    public List<Integer> timeFrame;
    public int year;
    public String brand;
    public List<String> technicalDetails;
    public Map<String, String> pictures;
    public boolean working = false;
}
