package fr.uavignon.ceri.tp4;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

public class SecondFragment extends Fragment {
    private SecondFragmentViewModel viewModel;
    private TextView itemName, itemCat, itemDesc, itemTF, itemYear, itemBrand, itemTD;
    private ImageView itemThumb;
    private ViewPager2 pager2;
    private ImageAdapter adapter;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(SecondFragmentViewModel.class);

        SecondFragmentArgs args = SecondFragmentArgs.fromBundle(getArguments());
        String itemId = args.getItemId();
        viewModel.setItem(itemId);

        itemName = getView().findViewById(R.id.detail_name);
        itemThumb = getView().findViewById(R.id.detail_thumb);
        itemCat = getView().findViewById(R.id.detail_categories);
        itemDesc = getView().findViewById(R.id.detail_description);
        itemTF = getView().findViewById(R.id.detail_timeframe);
        itemYear = getView().findViewById(R.id.detail_year);
        itemBrand = getView().findViewById(R.id.detail_brand);
        itemTD = getView().findViewById(R.id.detail_technicaldetails);

        pager2 = getView().findViewById(R.id.pager);
        adapter = new ImageAdapter();
        adapter.setViewModel(viewModel);
        pager2.setAdapter(adapter);

        Glide.with(getContext())
                .load("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+itemId+"/thumbnail")
                .into(itemThumb);

        listener();
        observer();
    }

    private void listener() {
        getView().findViewById(R.id.detail_button).setOnClickListener(
                view1 -> NavHostFragment.findNavController(SecondFragment.this).navigate(R.id.action_SecondFragment_to_FirstFragment)
        );
    }

    private void observer() {
        viewModel.getWebServiceThrowable().observe(getViewLifecycleOwner(),
                web -> Snackbar.make(getView(), web.getMessage(), Snackbar.LENGTH_LONG).show()
        );

        viewModel.getItem().observe(getViewLifecycleOwner(), item -> {
            if(item != null) {
                adapter.setItem(item);
                itemName.setText(item.getName());
                itemCat.setText("Catégories :\n"+item.getCategories());
                itemDesc.setText("Description :\n"+item.getDescription());
                itemTF.setText("Décennies d'utilisation :\n"+item.getTimeFrame());
                if(item.getYear() != 0)
                    itemYear.setText("Année d'apparition :\n"+String.valueOf(item.getYear()));
                else
                    itemYear.setText("Année d'apparition :\nInconnue");
                if(item.getBrand() == null || item.getBrand().isEmpty())
                    itemBrand.setText("Marque :\nInconnue");
                else
                    itemBrand.setText("Marque :\n"+item.getBrand());
                if(item.getTechnicalDetails() == null || item.getTechnicalDetails().isEmpty())
                    itemTD.setText("Caractéristiques techniques :\nInconnues");
                else
                    itemTD.setText("Caractéristiques techniques :\n"+item.getTechnicalDetails());
            }
        });
    }
}