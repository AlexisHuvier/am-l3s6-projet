package fr.uavignon.ceri.tp4;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import fr.uavignon.ceri.tp4.data.Item;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    private List<Item> items;
    private FirstFragmentViewModel viewModel;
    private boolean layout_left = true;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        if(layout_left)
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout, parent, false);
        else
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout_2, parent, false);
        layout_left = !layout_left;
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Item item = items.get(position);

        holder.itemName.setText(item.getName());
        holder.itemCategories.setText(item.getCategories());

        Glide.with(viewModel.getApplication())
                .load("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+item.getItemID()+"/thumbnail")
                .into(holder.itemThumb);
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public void setItems(List<Item> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public void setViewModel(FirstFragmentViewModel viewModel) {
        this.viewModel = viewModel;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemName, itemCategories;
        ImageView itemThumb;

        ViewHolder(View view) {
            super(view);

            itemName = view.findViewById(R.id.itemName);
            itemThumb = view.findViewById(R.id.image_thumb);
            itemCategories = view.findViewById(R.id.itemCategories);

            view.setOnClickListener((view1) -> {
                String id = RecyclerAdapter.this.items.get(getAdapterPosition()).getItemID();
                FirstFragmentDirections.ActionFirstFragmentToSecondFragment action = FirstFragmentDirections.actionFirstFragmentToSecondFragment(id);
                Navigation.findNavController(view1).navigate(action);
            });
        }
    }
}
