package fr.uavignon.ceri.tp4.data.webservice;


import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CMInterface {
    @GET("collection")
    Call<Map<String, CMCollectionResponse>> getCollection();
}
