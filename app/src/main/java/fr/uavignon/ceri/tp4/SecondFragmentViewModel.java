package fr.uavignon.ceri.tp4;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp4.data.CMRepository;
import fr.uavignon.ceri.tp4.data.Item;

public class SecondFragmentViewModel extends AndroidViewModel {
    private CMRepository repository;
    private MutableLiveData<Item> item;

    public SecondFragmentViewModel(Application application) {
        super(application);
        repository = CMRepository.get(application);
        item = new MutableLiveData<>();
    }

    public void setItem(String id) {
        repository.getItem(id);
        item = repository.getSelectedItem();
    }

    LiveData<Item> getItem() { return item; }

    LiveData<Throwable> getWebServiceThrowable() { return repository.getWebServiceThrowable(); }
}
