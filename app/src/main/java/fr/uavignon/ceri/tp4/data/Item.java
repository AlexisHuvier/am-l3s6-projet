package fr.uavignon.ceri.tp4.data;

import android.os.Build;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Entity(tableName = "item")
public class Item {
    @PrimaryKey()
    @NonNull
    private String itemID;

    @NonNull
    private String name;

    @NonNull
    private String categories;

    @NonNull
    private String description;

    @NonNull
    private String timeFrame;

    private int year;
    private String brand;
    private String technicalDetails;
    private String pictures;
    private boolean working;

    public Item(@NonNull String itemID, @NonNull String name, @NonNull String categories, @NonNull String description, @NonNull String timeFrame, int year, String brand, String technicalDetails, String pictures, boolean working) {
        this.itemID = itemID;
        this.name = name;
        this.categories = categories;
        this.description = description;
        this.timeFrame = timeFrame;
        this.year = year;
        this.brand = brand;
        this.technicalDetails = technicalDetails;
        this.pictures = pictures;
        this.working = working;
    }

    public Item(@NonNull String itemID, @NonNull String name, @NonNull List<String> categories, @NonNull String description, @NonNull List<Integer> timeFrame, int year, String brand, List<String> technicalDetails, Map<String, String> pictures, boolean working) {
        this.itemID = itemID;
        this.name = name;
        this.setCategories(categories);
        this.description = description;
        this.setTimeFrame(timeFrame);
        this.year = year;
        this.brand = brand;
        this.setTechnicalDetails(technicalDetails);
        this.setPictures(pictures);
        this.working = working;
    }

    @NonNull
    public String getItemID() {
        return itemID;
    }

    public void setItemID(@NonNull String itemID) {
        this.itemID = itemID;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public List<String> getTransformedCategories() {
        return Arrays.asList(categories.split(", "));
    }

    @NonNull
    public String getCategories() {
        return categories;
    }

    public void setCategories(@NonNull List<String> categories) {
        this.categories = TextUtils.join(", ", categories);
    }

    public void setCategories(@NonNull String categories) {
        this.categories = categories;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    @NonNull
    public String getTimeFrame() {
        return timeFrame;
    }

    @NonNull
    public List<Integer> getTransformedTimeFrame() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Arrays.stream(timeFrame.split(", ")).map(Integer::valueOf).collect(Collectors.toList());
        }
        else { // Compatibility with old versions
            List<Integer> list = new ArrayList<>();
            for (String text: timeFrame.split(", ")) {
                list.add(Integer.valueOf(text));
            }
            return list;
        }
    }

    public void setTimeFrame(@NonNull String timeFrame) {
        this.timeFrame = timeFrame;
    }

    public void setTimeFrame(@NonNull List<Integer> timeFrame) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            this.timeFrame = TextUtils.join(", ", timeFrame.stream().map(String::valueOf).collect(Collectors.toList()));
        }
        else { // Compatibility with old versions
            List<String> list = new ArrayList<>();
            for (int nb: timeFrame) {
                list.add(String.valueOf(nb));
            }
            this.timeFrame = TextUtils.join(", ", list);
        }
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public List<String> getTransformedTechnicalDetails() {
        return Arrays.asList(technicalDetails.split(", "));
    }

    public String getTechnicalDetails() {
        return technicalDetails;
    }

    public void setTechnicalDetails(List<String> technicalDetails) {
        this.technicalDetails = TextUtils.join(", ", technicalDetails);
    }

    public void setTechnicalDetails(String technicalDetails) {
        this.technicalDetails = technicalDetails;
    }

    public HashMap<String, String> getTransformedPictures() {
        HashMap<String, String> pics = new HashMap<>();
        for(String picture: pictures.split("\\|")) {
            if(picture.contains("$"))
                pics.put(picture.split("\\$")[0], picture.split("\\$")[1]);
            else
                pics.put(picture, null);
        }
        return pics;
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(Map<String, String> pictures) {
        List<String> pics = new ArrayList<>();
        for(String key: pictures.keySet())
            pics.add(key+"$"+pictures.get(key));
        this.pictures = TextUtils.join("|", pics);
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }

    public boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }
}
