package fr.uavignon.ceri.tp4;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

public class FirstFragment extends Fragment {
    private FirstFragmentViewModel viewModel;

    private SearchView searchView;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_update) {
            AsyncTask.execute(viewModel::updateCollection);
            return true;
        }
        else if(id == R.id.action_alpha) {
            viewModel.sortAlpha();
            viewModel.getAllItems().observe(getViewLifecycleOwner(), items -> adapter.setItems(items));
            return true;
        }
        else if(id == R.id.action_chrono) {
            viewModel.sortChrono();
            viewModel.getAllItems().observe(getViewLifecycleOwner(), items -> adapter.setItems(items));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(FirstFragmentViewModel.class);

        searchView = getView().findViewById(R.id.search);
        recyclerView = getView().findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter();
        recyclerView.setAdapter(adapter);
        adapter.setViewModel(viewModel);

        listenerSetup();
        observerSetup();

    }

    private void listenerSetup() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                viewModel.search(query);
                viewModel.getAllItems().observe(getViewLifecycleOwner(), items -> adapter.setItems(items));
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnCloseListener(() -> {
            viewModel.reset();
            viewModel.getAllItems().observe(getViewLifecycleOwner(), items -> adapter.setItems(items));
            return false;
        });
    }

    private void observerSetup() {
        viewModel.getWebServiceThrowable().observe(getViewLifecycleOwner(), webServiceThrowable -> {
            Snackbar.make(getView(), webServiceThrowable.getMessage(), Snackbar.LENGTH_LONG).show();
        });

        viewModel.getAllItems().observe(getViewLifecycleOwner(), items -> adapter.setItems(items));
    }
}