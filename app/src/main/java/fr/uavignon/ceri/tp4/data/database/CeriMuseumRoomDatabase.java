package fr.uavignon.ceri.tp4.data.database;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.uavignon.ceri.tp4.data.Item;

@Database(entities = {Item.class}, version = 1, exportSchema = false)
public abstract class CeriMuseumRoomDatabase extends RoomDatabase {

    private static final String TAG = CeriMuseumRoomDatabase.class.getSimpleName();

    public abstract ItemDao itemDao();

    private static CeriMuseumRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public static CeriMuseumRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (CeriMuseumRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    // without populate
                        /*
                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    WeatherRoomDatabase.class,"book_database")
                                    .build();

                     */

                    // with populate
                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    CeriMuseumRoomDatabase.class,"cerimuseum_database")
                                    .addCallback(sRoomDatabaseCallback)
                                    .build();

                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);

                    databaseWriteExecutor.execute(() -> {
                        // Populate the database in the background.
                        ItemDao dao = INSTANCE.itemDao();
                    });

                }
            };



}
