package fr.uavignon.ceri.tp4;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp4.data.CMRepository;
import fr.uavignon.ceri.tp4.data.Item;

public class FirstFragmentViewModel extends AndroidViewModel {

    private CMRepository repository;
    private LiveData<List<Item>> allItems;

    public FirstFragmentViewModel (Application application) {
        super(application);
        repository = CMRepository.get(application);
        allItems = repository.getAllitems();
    }

    public void updateCollection() {
        repository.updateCollection();
    }

    public void sortAlpha() {
        repository.sortAlpha();
        allItems = repository.getAllitems();
    }

    public void search(String search) {
        repository.search(search);
        allItems = repository.getAllitems();
    }

    public void sortChrono() {
        repository.sortChrono();
        allItems = repository.getAllitems();
    }

    public void reset() {
        repository.reset();
        allItems = repository.getAllitems();
    }

    LiveData<List<Item>> getAllItems() {
        return allItems;
    }

    LiveData<Throwable> getWebServiceThrowable() { return repository.getWebServiceThrowable(); }
}
